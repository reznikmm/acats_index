--  SPDX-FileCopyrightText: 2020 Max Reznik <reznikmm@gmail.com>
--
--  SPDX-License-Identifier: MIT
----------------------------------------------------------------

with Ada.Directories;
with Ada.Environment_Variables;
with Ada.Wide_Wide_Text_IO;

with League.Application;
with League.Strings;
with League.String_Vectors;

with Def_Name;
with Processes;

procedure Main is
   use type League.Strings.Universal_String;

   function "+"
     (Item : Wide_Wide_String) return League.Strings.Universal_String
      renames League.Strings.To_Universal_String;

   procedure Create_Directories;
   procedure Build_Macrosub;
   procedure Prepare_Support;
   procedure Write_File
     (File : League.Strings.Universal_String;
      Text : League.Strings.Universal_String);
   procedure Copy_File
     (File : League.Strings.Universal_String;
      Dir  : League.Strings.Universal_String);
   procedure Chop
     (File : League.Strings.Universal_String;
      Dir  : League.Strings.Universal_String);
   procedure Run_Macrosub (Dir : League.Strings.Universal_String);
   procedure Process_Dir (Dir : League.Strings.Universal_String);
   procedure Process_Test
     (Dir  : League.Strings.Universal_String;
      Test : League.Strings.Universal_String);
   procedure Make_Trees (Dir : League.Strings.Universal_String);

   function GNAT_Path return League.Strings.Universal_String;

   Path_1 : League.Strings.Universal_String;

   Root  : constant League.Strings.Universal_String := +"/tmp";

   Support : constant League.Strings.Universal_String :=
     Root & "/support";
   Macrosub : constant League.Strings.Universal_String :=
     Root & "/macrosub";

   ACATS_Dir : constant League.Strings.Universal_String :=
     League.Application.Arguments.Element (1);

   ----------
   -- Chop --
   ----------

   procedure Chop
     (File : League.Strings.Universal_String;
      Dir  : League.Strings.Universal_String)
   is
      Chop_Args : League.String_Vectors.Universal_String_Vector;
      Output    : League.Strings.Universal_String;
      Errors    : League.Strings.Universal_String;
      Status    : Integer;
   begin
      Chop_Args.Append (+"-r");
      Chop_Args.Append (File);

      Processes.Run
        (Program   => GNAT_Path & "/gnatchop",
         Arguments => Chop_Args,
         Directory => Dir,
         Output    => Output,
         Errors    => Errors,
         Status    => Status);

      if Status /= 0 then
         Ada.Wide_Wide_Text_IO.Put_Line (Output.To_Wide_Wide_String);
         Ada.Wide_Wide_Text_IO.Put_Line (Errors.To_Wide_Wide_String);
         raise Program_Error;
      end if;
   end Chop;

   function GNAT_Path return League.Strings.Universal_String is
   begin
      if Path_1.Is_Empty then
         declare
            Path : constant League.Strings.Universal_String :=
              League.Strings.From_UTF_8_String
                (Ada.Environment_Variables.Value ("PATH"));
         begin
            Path_1 := Path.Split (':').Element (1);
         end;
      end if;

      return Path_1;
   end GNAT_Path;

   procedure Build_Macrosub is
      GPR_Args  : League.String_Vectors.Universal_String_Vector;
      Output    : League.Strings.Universal_String;
      Errors    : League.Strings.Universal_String;
      Status    : Integer;
   begin
      Chop (ACATS_Dir & "/SUPPORT/MACROSUB.ADA", Macrosub);
      GPR_Args.Append (+"macrosub.adb");

      Processes.Run
        (Program   => GNAT_Path & "/gprbuild",
         Arguments => GPR_Args,
         Directory => Macrosub,
         Output    => Output,
         Errors    => Errors,
         Status    => Status);

      if Status /= 0 then
         Ada.Wide_Wide_Text_IO.Put_Line (Output.To_Wide_Wide_String);
         Ada.Wide_Wide_Text_IO.Put_Line (Errors.To_Wide_Wide_String);
         raise Program_Error;
      end if;

      Ada.Wide_Wide_Text_IO.Put_Line ("Build macrosub: done");
   end Build_Macrosub;

   procedure Copy_File
     (File : League.Strings.Universal_String;
      Dir  : League.Strings.Universal_String)
   is
      Name : constant String := File.To_UTF_8_String;
   begin
      Ada.Directories.Copy_File
        (Name,
         Dir.To_UTF_8_String & "/" & Ada.Directories.Simple_Name (Name));
   end Copy_File;

   procedure Create_Directories is
   begin
      Ada.Directories.Create_Path (Support.To_UTF_8_String);
      Ada.Directories.Create_Path (Macrosub.To_UTF_8_String);
   end Create_Directories;

   procedure Make_Trees (Dir : League.Strings.Universal_String) is
      procedure Each_File (File : Ada.Directories.Directory_Entry_Type);

      Args : League.String_Vectors.Universal_String_Vector;

      procedure Each_File (File : Ada.Directories.Directory_Entry_Type) is
         Name : constant League.Strings.Universal_String :=
           League.Strings.From_UTF_8_String
             (Ada.Directories.Simple_Name (File));
         ADS  : constant League.Strings.Universal_String :=
           Name.Head (Name.Length - 1) & "s";
         Index : constant Natural := Args.Index (ADS);
      begin
         if Index > 0 then
            Args.Replace (Index, Name);
         else
            Args.Append (Name);
         end if;
      end Each_File;

      Output    : League.Strings.Universal_String;
      Errors    : League.Strings.Universal_String;
      Status    : Integer;
   begin
      Args.Append (+"-c");
      Args.Append (+"-gnatct");
      Args.Append (+"-I");
      Args.Append (Support);

      Ada.Directories.Search
        (Directory => Dir.To_UTF_8_String,
         Pattern   => "*.ad*",
         Process   => Each_File'Access);

      Processes.Run
        (Program   => GNAT_Path & "/gcc",
         Arguments => Args,
         Directory => Dir,
         Output    => Output,
         Errors    => Errors,
         Status    => Status);

      if Status /= 0 then
         Ada.Wide_Wide_Text_IO.Put_Line (Output.To_Wide_Wide_String);
         Ada.Wide_Wide_Text_IO.Put_Line (Errors.To_Wide_Wide_String);
         raise Program_Error;
      end if;
   end Make_Trees;

   procedure Prepare_Support is
   begin
      Copy_File
        (ACATS_Dir & "/SUPPORT/SPPRT13S.TST",
         Support);
      Write_File (Support & "/TSTTESTS.DAT", +"SPPRT13S.TST");
      Run_Macrosub (Support);

      Chop (+"SPPRT13S.ADT", Support);
      Chop (ACATS_Dir & "/SUPPORT/REPORT.A", Support);
      Chop (ACATS_Dir & "/SUPPORT/IMPDEF.A", Support);
      Chop (ACATS_Dir & "/SUPPORT/FCNDECL.ADA", Support);
      Chop (ACATS_Dir & "/SUPPORT/CHECKFIL.ADA", Support);
      Chop (ACATS_Dir & "/SUPPORT/LENCHECK.ADA", Support);
      Chop (ACATS_Dir & "/SUPPORT/ENUMCHEK.ADA", Support);
      Chop (ACATS_Dir & "/SUPPORT/TCTOUCH.ADA", Support);

      Ada.Wide_Wide_Text_IO.Put_Line ("Prepare support: done");
   end Prepare_Support;

   procedure Process_Dir (Dir : League.Strings.Universal_String) is
      procedure Each_File (File : Ada.Directories.Directory_Entry_Type);

      Path : constant League.Strings.Universal_String :=
        ACATS_Dir & "/" & Dir;

      List : League.String_Vectors.Universal_String_Vector;

      procedure Each_File (File : Ada.Directories.Directory_Entry_Type) is
         Name : constant String := Ada.Directories.Simple_Name (File);
         Test : constant League.Strings.Universal_String :=
           League.Strings.From_UTF_8_String (Name (1 .. 7));
      begin
         if List.Index (Test) = 0 then
            List.Append (Test);
         end if;
      end Each_File;
   begin
      Ada.Directories.Search
        (Directory => Path.To_UTF_8_String,
         Pattern   => "",
         Filter    => (Ada.Directories.Ordinary_File => True,
                       others                        => False),
         Process   => Each_File'Access);

      for J in 1 .. List.Length loop
         Process_Test (Dir, List (J));
      end loop;
   end Process_Dir;

   procedure Process_Test
     (Dir  : League.Strings.Universal_String;
      Test : League.Strings.Universal_String)
   is
      procedure Each_File (File : Ada.Directories.Directory_Entry_Type);

      Path : constant League.Strings.Universal_String :=
        ACATS_Dir & "/" & Dir;

      Out_Dir : constant League.Strings.Universal_String :=
        Root & "/run/" & Test;

      procedure Each_File (File : Ada.Directories.Directory_Entry_Type) is
         ADT : League.Strings.Universal_String;
         Name : constant League.Strings.Universal_String :=
           League.Strings.From_UTF_8_String
             (Ada.Directories.Simple_Name (File));
      begin
         Ada.Wide_Wide_Text_IO.Put_Line (Name.To_Wide_Wide_String);

         if Name.Ends_With (".ADA") then
            Chop (Path & "/" & Name, Out_Dir);
         elsif Name.Ends_With (".TST") then
            Copy_File
              (Path & "/" & Name,
               Out_Dir);
            Write_File (Out_Dir & "/TSTTESTS.DAT", Name);
            Run_Macrosub (Out_Dir);
            ADT := Out_Dir & "/" & Name.Head (Name.Length - 4) & ".ADT";
            Chop (ADT, Out_Dir);
            Ada.Directories.Delete_File (ADT.To_UTF_8_String);
         else
            raise Program_Error;
         end if;

      end Each_File;
   begin
      Ada.Directories.Create_Path (Out_Dir.To_UTF_8_String);

      Ada.Directories.Search
        (Directory => Path.To_UTF_8_String,
         Pattern   => Test.To_UTF_8_String & "*",
         Filter    => (Ada.Directories.Ordinary_File => True,
                       others                        => False),
         Process   => Each_File'Access);

      Make_Trees (Out_Dir);

      Write_File
        (Root & "/run/" & Test & ".txt",
         Def_Name (Out_Dir));
   end Process_Test;

   procedure Run_Macrosub (Dir : League.Strings.Universal_String) is
      Output : League.Strings.Universal_String;
      Errors : League.Strings.Universal_String;
      Status : Integer;
   begin
      Copy_File
        (ACATS_Dir & "/SUPPORT/MACRO.DFS",
         Dir);

      Processes.Run
        (Program   => Macrosub & "/macrosub",
         Arguments => League.String_Vectors.Empty_Universal_String_Vector,
         Directory => Dir,
         Output    => Output,
         Errors    => Errors,
         Status    => Status);

      if Status /= 0 then
         Ada.Wide_Wide_Text_IO.Put_Line (Output.To_Wide_Wide_String);
         Ada.Wide_Wide_Text_IO.Put_Line (Errors.To_Wide_Wide_String);
         raise Program_Error;
      end if;

      Ada.Directories.Delete_File (Dir.To_UTF_8_String & "/MACRO.DFS");
      Ada.Directories.Delete_File (Dir.To_UTF_8_String & "/TSTTESTS.DAT");

      Ada.Wide_Wide_Text_IO.Put_Line
        ("Run macrosub in " & Dir.To_Wide_Wide_String);
   end Run_Macrosub;

   procedure Write_File
     (File : League.Strings.Universal_String;
      Text : League.Strings.Universal_String)
   is
      Output : Ada.Wide_Wide_Text_IO.File_Type;
   begin
      Ada.Wide_Wide_Text_IO.Create (Output, Name => File.To_UTF_8_String);
      Ada.Wide_Wide_Text_IO.Put_Line (Output, Text.To_Wide_Wide_String);
      Ada.Wide_Wide_Text_IO.Close (Output);
   end Write_File;

begin
   if League.Application.Arguments.Length > 1 then
      Create_Directories;
      Build_Macrosub;
      Prepare_Support;
   end if;
   Process_Dir (+"A");
end Main;
