--  SPDX-FileCopyrightText: 2020 Max Reznik <reznikmm@gmail.com>
--
--  SPDX-License-Identifier: MIT
----------------------------------------------------------------

with League.Strings;

function Def_Name (Dir : League.Strings.Universal_String)
  return League.Strings.Universal_String;
