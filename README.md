# A tool to make xrefs for ACATS

Download and unpack acats

    wget http://www.ada-auth.org/acats-files/4.1/ACATS41.ZIP
    unzip -aa ACATS41.ZIP -d /tmp/acats

Compile and run the tool

    .objs/main /tmp/acats

Result xrefs are stored in text files in `/tmp/run`.
